"""Log channel communication."""
import datetime
import json
import sys
from gitterpy.client import GitterClient
from logitter import conf

msgs = []
heading = "Meeting"

def is_command(m):
    """Return command of the message.

    Keyword arguments:
    m -- Message.
    """
    splitted = m["text"].split(' ', 1)
    if splitted[0][0] == ";":
        return {
            "cmd": splitted[0][1:],
            "text": splitted[1],
        }
    return False

def message_text_short(m):
    """Return display name and text of stream message.

    Keyword arguments:
    m -- Stream message in JSON format.
    """
    s = m["sent"]
    dn = m["fromUser"]["displayName"]
    t = m["text"]
    return "{} {}: {}".format(s, dn, t)

def messages_to_md(ml):
    """Parse messages list ``ml``.

    Keyword arguments:
    ml -- Messages list.
    """
    intro = []
    todo = []
    topic = False
    discussed = {}
    for m in ml:
        c = is_command(m)
        if not c:
           continue
        if c["cmd"] == "iam":
            intro.append("{} -- {}".format(
                m["fromUser"]["displayName"],
                c["text"],
            ))
        elif c["cmd"] == "iwill":
            text = "{} will {}".format(
                m["fromUser"]["displayName"],
                c["text"],
            )
            todo.append(text)
            if topic:
                discussed[topic].append(text)
        elif c["cmd"] == "discuss":
            topic = c["text"]
            discussed[topic] = []
        elif c["cmd"] == "note":
            text = c["text"]
            if topic:
                discussed[topic].append(text)
    md = ""
    if len(intro) > 0:
        md += "# Introduction\n"
        for u in intro:
            md += "- {}\n".format(u)
        md += "\n"
    for (topic, log) in discussed.items():
        md += "# {}\n".format(topic)
        for i in log:
            md += "- {}\n".format(i)
        md += "\n"
    if len(todo) > 0:
        md += "# TODO\n"
        for i in todo:
            md += "- {}\n".format(i)
        md += "\n"
    return md

def parse_sm(sm):
    """Parse stream message.

    Keyword arguments:
    sm -- Stream message.
    """
    if sm == b' ':
        return
    m = json.loads(sm)
    save_sm(m)
    print(message_text_short(m))

def save_sm(m):
    """Save stream message to ``msgs`` global list.

    Keyword arguments:
    m -- Stream message in JSON format.
    """
    global msgs
    msgs.append(m)

def write_logfile(ml, fn):
    """Write messages to the log file.

    Keyword arguments:
    ml -- Messages list.
    fn -- File name.
    """
    with open("{}".format(fn), "w") as f:
        f.write("# {}\n".format(heading))
        dt = datetime.datetime.now()
        f.write("The date of the meeting is {}.\n".format(dt))
        f.write("\n")
        f.write(messages_to_md(ml))
        f.write("# Raw messages log\n")
        f.write("```\n")
        for m in ml:
            f.write("{}\n".format(message_text_short(m)))
        f.write("```\n")

if __name__ == "__main__":
    g = GitterClient(conf.GITTER_TOKEN)
    if len(sys.argv) == 2:
        heading = sys.argv[1]
    try:
        g.messages.send(conf.GITTER_ROOM, "# {}".format(heading))
        g.messages.send(
            conf.GITTER_ROOM,
            """Logitter use the following commands:
- `;iam a developer` to introduce yourself.
- `;iwill do the task` to assign yourself the task.
- `;discuss Logitter commands` to change the topic.
- `;note There is new ;note command` to make a note.
            """,
        )
        for sm in g.stream.chat_messages(conf.GITTER_ROOM).iter_lines():
            if sm:
                parse_sm(sm)
    except KeyboardInterrupt:
        fn = datetime.datetime.now().isoformat()
        write_logfile(msgs, "{}.md".format(fn))
