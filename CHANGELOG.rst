Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_ and this project adheres to
`Semantic Versioning`_.

.. _Keep a Changelog: http://keepachangelog.com/
.. _Semantic Versioning: http://semver.org/

`Unreleased`_
-------------

Added
^^^^^

- Markdown output of meeting.

- There are ``;iam``, ``;iwill``, ``;discuss``, and ``;note`` commands
  available now.

0.1.0 - 2019-12-20
------------------

Added
^^^^^

- Changelog, license, readme.

- Log messages to file when Logitter finishes.


.. _Unreleased: https://gitlab.com/qeef/logitter/compare/v0.1.0...master
