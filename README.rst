Logitter
========

**Logitter** logs messages from gitter channel.

License
-------

The project is published under `MIT License`_.

.. _MIT License: ./LICENSE

Run Logitter
------------

1. Set ``GITTER_TOKEN`` (see `gitter devs`_) and ``GITTER_ROOM`` (like
   *community/room*) in ``conf.py`` file.

2. Create virtual environment, activate, and install requirements::

    virtualenv -p python3 tve
    . tve/bin/activate
    pip install -r requirements.txt

3. Run **Logitter**::

    PYTHONPATH=. python3 logitter/log.py

.. _gitter devs: https://developer.gitter.im/apps

Logitter commands
-----------------

There is a list of commands to be used in a gitter chat room. All the commands
start with ``;``.

``;iam``
    Introduce yourself.

    Example: ``;iam a developer``

``;iwill``
    Make yourself willing to do a task.

    Example: ``;iwill do the task``

``;discuss``
    Talk about some topic.

    Example: ``;discuss Logitter commands``

``;note``
    Make note about some information.

    Example: ``;note There is new ;note command``

Contribute
==========

Use `OneFlow`_ branching model and keep the `changelog`_.

Write `great git commit messages`_:

1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

.. _OneFlow: https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
.. _changelog: ./CHANGELOG.rst
.. _great git commit messages: https://chris.beams.io/posts/git-commit/
